import time

class VendingMachine(object):


    def __init__(self):
        self.validcoins = {"nickel": 10, "dime": 20, "quarter": 25}
        self.balance = 0
        self.products = {"cola": 100, "crisps": 50, "chocolate": 65}


    def UpdateBalance(self, new_message):
        print('\nYour current balance is: $', (new_message/100))
        return new_message


    def AcceptCoins(self):
        product = self.ChooseProduct()
        print('\nYour choice of', product, 'costs $', self.products[product]/100)
        
        while self.balance < self.products[product]:
            print('You owe $', (self.products[product] - self.balance)/100)
            coin = input("Please insert a coin (type nickel, dime or quarter): ")
            
            if coin in self.validcoins.keys():
                self.balance += self.validcoins[coin]
                self.UpdateBalance(self.balance)
            elif coin == 'penny':
                print('\nWe do not accept pennies, please try again')
                continue
            else:
                print('\nInvalid input, please try again')
                continue

        self.GiveChange(self.balance, product)
        print('Thank you!')
        self.balance = 0
        time.sleep(1)
        self.AcceptCoins()


    def GiveChange(self, balance, product):
        if balance > self.products[product]:
            change_due = balance - self.products[product]
            print("Your change is", change_due/100)


    def DisplayProducts(self):
        print('\nWelcome to the Vending Machine! Choose an item from the following list:\n')
        for key, value in self.products.items():
            print(key, value/100)
        print('')


    def ChooseProduct(self):
        self.DisplayProducts()
        product = input("Please choose a product: ")
        if product in self.products.keys():
            return product


if __name__ == '__main__':
    V = VendingMachine()
    V.AcceptCoins()
