import random


class bowling():

    def __init__(self):
        self.score_track = {}

    def roll(self):
        score_lst = []
        knock_down_pins_first = random.randint(0, 10)
        score_lst.append(knock_down_pins_first)
        knock_down_pins_second = random.randint(0, 10 - knock_down_pins_first)
        score_lst.append(knock_down_pins_second)
        print(score_lst)
        return score_lst

    def update_score(self, frame, score):
        self.score_track[frame] = [score]
        if frame == 1:
            self.score_track[1].append(score)
        else:
            first_roll = self.score_track[frame - 1][0][0]
            both_rolls = (self.score_track[frame - 1][0][0]+self.score_track[frame-1][0][1])
            score_copy = score.copy()

            # strike
            if first_roll % 10 == 0 and first_roll != 0:
                score_copy = [2 * x for x in score_copy]
                self.score_track[frame].append(score_copy)

            # spare
            elif both_rolls % 10 == 0 and both_rolls != 0:
                score_copy[0] = score_copy[0]*2
                self.score_track[frame].append(score_copy)

            # neither
            else:
                self.score_track[frame].append(score_copy)

        # strike on 10th frame
        if frame == 10 and score[0] % 10 == 0 and score[0] != 0:
            print('Strike in 10th frame')
            extra_roll_count = 1
            extra_score = 0
            extra_pin = random.randint(0, 10)
            extra_score += extra_pin
            print(extra_roll_count, 'extra pin:', extra_pin)
            while extra_roll_count < 3 and extra_pin == 10:
                extra_pin = random.randint(0, 10)
                extra_score += extra_pin
                extra_roll_count += 1
                print(extra_roll_count, 'extra pin:', extra_pin)
            self.score_track[frame][1].append(extra_score)
            print('Extra score:', extra_score)

        # spare on 10th frame
        elif frame == 10 and sum(score) % 10 == 0 and sum(score) != 0:
            print('Spare in 10th frame')
            extra_score = 0
            extra_pin = random.randint(0, 10)
            extra_score += extra_pin

            self.score_track[frame][1].append(extra_score)
            print('Extra score:', extra_score)

        print(self.score_track)
        return self.score_track

    def total_score(self, score_track):
        total = 0
        for key, value in score_track.items():
            total += sum(value[1])
        return total


if __name__ == '__main__':

    game = bowling()

    for frame in range(1, 11):
        print('Frame', frame)
        game.update_score(frame, game.roll())

    print('Congratulations! Your final score is:', game.total_score(game.score_track))